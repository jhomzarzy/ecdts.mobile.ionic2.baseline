export enum ConfigEnum {
    host = 'http://ecdtsapi.bfar.da.gov.ph:9092/api/',
    client_id= 'b8c7adb6-37e3-4b08-87be-5a1e448be6cf',
    secret_key= 'dda3e353-6b6f-493c-8ad4-fe5a86ba19f2',
    client_name= 'bfar-ecdts-webapp',

    ecdts_data_service= 'bfar-ecdts-data-api/v1',
    bfar_login_service= 'bfar-login-api/v1',
    bfar_attach_service= 'bfar-attachment-api/v1',
    bfar_common_service= 'bfar-common-data-api/v1',
    bfar_felis_service= 'bfar-felis-data-api/v1',
    bfar_auth_service= 'bfar-auth-api/v1/',
}