import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs';

import  * as CryptoJS from 'crypto-js';
import  * as moment   from 'moment';

import { LocalStorageProvider } from '../../providers/shared-providers/local-storage/local-storage';

@Injectable()
export class Rest{
    http:any;
    baseUrl: String;
    baseHost: String;

    constructor(http:Http, private localstorageprovider: LocalStorageProvider){
        this.http = http;
        // this.baseUrl = 'http://172.16.1.7:8081';
        // // this.baseUrl = 'http://192.168.1.135:8081';
        this.baseUrl = 'https://ecdtsapi.bfar.da.gov.ph'
        console.log('Hello rest Service');
    }

    data(method, url, data){
        // let headers = new Headers();

        let dateTimeNow = this.getDateTime();

        let body = this.encryptBody(data);

        let enc = this.generateEncryptStringToSign(method,body,dateTimeNow,url)

        let headers = new Headers();
        this.generateRequestHeader(headers,enc,body,dateTimeNow);

        // console.log("HEADER : \n",headObj);

        console.log(method + ":" + url + data);

        if(method == 'POST'){
            return this.doPOST(url,data,headers);
        }else if(method == 'GET'){
            return this.doGET(url,headers);
        }else if(method == 'PUT'){
            return this.doPUT(url,data,headers);
        }else if(method == 'DELETE'){
            return this.doDELETE(url,data,headers);
        }else{
            return "Method Type Not Found";
        }
    }
    
    private doPOST(url,data,headObj){
        return this.http.post(
                    this.baseUrl+""+url, 
                    JSON.stringify(data),
                    {headers:headObj})
                .map(this.extractData) 
                .catch(this.handleErrorObservable);
    }
    
    private doGET(url,headObj){
        return this.http.get(
                    this.baseUrl+""+url,
                    {headers:headObj},
                    {"qwe":'qwe'})
                .map(this.extractData) 
                .catch(this.handleErrorObservable);
    }
    
    private doPUT(url,data,headObj){
        return this.http.put(
                    this.baseUrl+""+url, 
                    JSON.stringify(data),
                    {headers:headObj})
                .map(this.extractData) 
                .catch(this.handleErrorObservable);
    }
    
    private doDELETE(url,data,headObj){
        return this.http.delete(
                    this.baseUrl+""+url, 
                    JSON.stringify(data),
                    {headers:headObj})
                .map(this.extractData) 
                .catch(this.handleErrorObservable);
    }

    private extractData(res: Response) {
	    let body = res.json();
        return body || {};
    }
    private handleErrorObservable (error: Response | any) {
	    console.error(error.message || error);
	    return Observable.throw(error.message || error);
    }
    
    private getDateTime (){
        return moment().format('DD/MM/YYYY') + " " + moment().format('HH:mm:ss');
    }

    private encryptBody (data){
        let body = '';
        if(data != null){
            body = CryptoJS.enc.Hex.stringify(CryptoJS.MD5(JSON.stringify(data))).toString(CryptoJS.enc.Utf8);
        }
        return body;
    }

    private generateEncryptStringToSign(method,body,dateTimeNow,url){
        let stringToSign = method + "\n"
                        + "application/json" + "\n"
                        + body + "\n"
                        + dateTimeNow + "\n"
                        + encodeURI(url.replace(this.baseUrl,"/"));
        return CryptoJS.enc.Hex.stringify(CryptoJS.HmacSHA512(stringToSign, "mysupersecret")).toString(CryptoJS.enc.Utf8);;
    }

    generateRequestHeader(header:Headers, enc,body,dateTimeNow){
        header.append("Authorization", "hmac "  + enc);
        header.append("Content-Type", "application/json");
        header.append("Content-MD5", body);
        header.append("Request-Date", dateTimeNow);
        header.append("x-api-key","mysupersecret");
        header.append("x-user-token", this.localstorageprovider.getSession());
        // return {
        //             "Authorization": "hmac "  + enc,
        //             "Content-Type": "application/json",
        //             "Content-MD5": body,
        //             "Request-Date": dateTimeNow,
        //             "x-api-key":"mysupersecret",
        //             "x-user-token": this.localstorageprovider.getSession()
        // };
    }
}