import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs';

import  * as CryptoJS from 'crypto-js';
import  * as moment   from 'moment';

import { ConfigEnum } from './Config';

@Injectable()
export class Auth{
    http:any;
    baseUrl: String;
    baseHost: String;

    constructor(http:Http){
        this.http = http;
        this.baseUrl = ConfigEnum.host;
        console.log('Hello auth Service');
    }

    
    authenticate(){
        // let headers = new Headers();
        // this.generateRequestHeader(headers);

        return this.http.get(
            this.baseUrl+ConfigEnum.bfar_auth_service+ "auth/authenticate?client_id=" + ConfigEnum.client_id + "&secret_key=" + ConfigEnum.secret_key,
            {headers:{
                "Content-Type": "application/json",
                "x-system-name": ConfigEnum.client_name,
                "Request-Date": this.getDateTime()
            }})
        .map(this.extractData) 
        .catch(this.handleErrorObservable);
    }

    // generateRequestHeader(header:Headers){
    //     header.append("Content-Type", "application/json");
    //     header.append("Request-Date", this.getDateTime());
    //     header.append("x-system-name", ConfigEnum.client_name);
    // }

    getDateTime (){
        return moment().format('DD/MM/YYYY') + " " + moment().format('HH:mm:ss');
    }

    private extractData(res: Response) {
	    let body = res.json();
        return body || {};
    }
    private handleErrorObservable (error: Response | any) {
	    //console.error(error.message || error);
	    return Observable.throw(error.message || error);
    }

}