import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';

//services
import { Rest } from './services/rest';

import { LoginPage } from '../pages/login/login'
import { HomePage } from '../pages/home/home';

//Shared Providers
import { LocalStorageProvider } from '../providers/shared-providers/local-storage/local-storage';
import { Base64converterProvider } from '../providers/shared-providers/base64converter/base64converter';
import { MsgpackLiteProvider } from '../providers/shared-providers/msgpack-lite/msgpack-lite';

//Resource Providers
import { LoginResourceProvider } from '../providers/resource-providers/login-resource/login-resource';

//Pipes
import { SortByPipe } from '../pipes/sort-by/sort-by';

import { Auth } from './services/auth';


@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    HomePage,
    SortByPipe

  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Auth,
    Rest,
    LocalStorageProvider,
    Base64converterProvider,
    MsgpackLiteProvider,
    LoginResourceProvider,
  ]
})
export class AppModule {}
