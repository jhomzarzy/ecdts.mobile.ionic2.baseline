import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//services
import { Rest } from './services/rest';

//pages
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';

//providers
import { LocalStorageProvider } from '../providers/shared-providers/local-storage/local-storage';


@Component({
  templateUrl: 'app.html', 
  providers: [Rest]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage:any = LoginPage;

  pages: Array<{title: string, component: any, ios :any, class:any}>;

  constructor(public platform: Platform,
    public events: Events,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public localstorage: LocalStorageProvider) {
      this.initializeApp();
      this.initializePages();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    console.log("lipat page");
    this.nav.setRoot(page.component);
  }

  initializePages(){

    this.pages = [
      { title: 'Home', component: HomePage, ios : 'ios-home', class : 'md-home'},
      { title: 'Log out', component: LoginPage, ios : 'ios-log-out', class : 'md-log-out'},
    ];
  }
}

