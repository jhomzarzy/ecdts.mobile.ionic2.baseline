import { NgModule } from '@angular/core';
import { SortByPipe } from './sort-by/sort-by';
import { FilterPipe } from './filter/filter';
@NgModule({
	declarations: [SortByPipe,
    FilterPipe],
	imports: [],
	exports: [SortByPipe,
    FilterPipe]
})
export class PipesModule {}
