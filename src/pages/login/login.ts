import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController} from 'ionic-angular';

//pages
import { HomePage } from '../../pages/home/home';

//http providers
import { LoginResourceProvider } from '../../providers/resource-providers/login-resource/login-resource';

//shared-providers
import { LocalStorageProvider } from '../../providers/shared-providers/local-storage/local-storage';

//sqlite providers
// import { UserServiceProvider } from '../../providers/sqlite-providers/user-service/user-service';
// import {UserInformationServiceProvider } from '../../providers/sqlite-providers/user-information-service/user-information-service';
// import {CompanyServiceProvider} from '../../providers/sqlite-providers/company-service/company-service';
// import { EcdtsDataResourceProvider } from '../../providers/resource-providers/ecdts-data-resource/ecdts-data-resource';
// import { OperatorServiceProvider } from '../../providers/sqlite-providers/operator-service/operator-service';
// import { FelisDataResourceProvider } from '../../providers/resource-providers/felis-data-resource/felis-data-resource';

import { Auth } from '../../app/services/auth';

import * as moment from 'moment';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  user: any = {};
  loader: any;
  // loginAs: any;
  // buttonText: String;
  userInfo: any;
  // company: any;
  // municipal: any;
  // operator: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private loginresourceprovider: LoginResourceProvider,
    private localstorageprovider: LocalStorageProvider,
    private auth: Auth,
    private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    this.localstorageprovider.removeAuthToken();
    this.checkAuth();
  }

  checkAuth() {
    if (this.localstorageprovider.getAuthToken() == null || this.localstorageprovider.getAuthToken() == undefined) {
      this.auth.authenticate().subscribe(response => {
        this.localstorageprovider.setAuthToken(response);
        console.log("pasok", response);
      }, error => {
        console.log("hindi", error);
        this.checkAuth();
      });
    } else if (this.localstorageprovider.getAuthToken().expiresIn < moment().format('x')) {
      console.log("renew auth");
      this.localstorageprovider.removeAuthToken();
      this.checkAuth();
    } else {
      console.log("valid auth");
    }
  }

  validateCredentials() {
    if (this.user.password == null || this.user.username == null) {
      return false;
    } else {
        console.log("online log in");
        this.validateViaWeb();
    }
  }

  private validateViaWeb() {
    this.presentLoading("Authenticating");
    this.loginresourceprovider.authenticate(this.user).subscribe(response => {
      console.log("pasok : ", response);
      this.userInfo = response.data.userInfo;
      this.getUserInfoFromWeb(response)
    }, error => {
      console.log("hindi pasok : ", error);
      this.loader.dismiss();
    });
  }


  getUserInfoFromWeb(user){
    this.loginresourceprovider.findUserInfoByUserId(user.data.user.uuid).subscribe(response=>{
      console.log("user info From web, ", response);
      this.userInfo=response;
    });
          this.setUpLocalStorageViaWeb(user);
  }

  setUpLocalStorageViaWeb(response) {
    var activeUser = {
      municipal: this.userInfo.municipalId,
      uuid: response.data.user.uuid,
      username: response.data.user.username,
      userrole: response.data.user.userrole,
      usertype: response.data.user.usertype,
    };

    this.loader.dismiss();

    this.localstorageprovider.setSession(response.data.session.sessionId);
    this.localstorageprovider.setActiveUser(activeUser);
    this.localstorageprovider.setCreatedDate(response.data.session.createdDate);
    

    this.redirectPage();
  }

  private setUpLocalStorageViaSql(user) {
    var activeUser = {
      uuid: user.uuid,
      username: user.username,
      userrole: user.userrole,
      usertype: user.usertype,
      municipal: this.userInfo.municipalId,
    };

    this.localstorageprovider.setActiveUser(activeUser);
    this.redirectPage();
  }

  private redirectPage() {
    this.navCtrl.push(HomePage);
  }

  private presentLoading(message) {
    this.loader = this.loadingCtrl.create({
      content: message + "...",
      duration: 120000
    });
    this.loader.present();
  }

  presentAlert(title, subTitle) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subTitle,
      buttons: ['Ok, I get It!']
    });
    alert.present();
  }

}
