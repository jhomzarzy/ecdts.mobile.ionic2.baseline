import { Component } from '@angular/core';
import { NavController, ViewController, LoadingController, AlertController, ToastController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  user: any;
  userInfo: any;
  company: any;
  loader: any;
  fishingGroundlist: any=[];
  municipalLogsheetList: any=[];
  byCatchList: any =[];
  targetList: any = [];
  errorTrggr = false;

  constructor(public navCtrl: NavController,
              public viewCtrl: ViewController,
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              public toastCtrl: ToastController,) {
                
  }

  ionViewDidLoad() {

  }

}
