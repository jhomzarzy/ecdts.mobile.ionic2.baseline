import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import  * as CryptoJS from 'crypto-js';
/*
  Generated class for the Base64converterProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class Base64converterProvider {

  constructor(public http: Http) {
    console.log('Hello Base64converterProvider Provider');
  }

  decryptData = function(data){
    let parsedStr = "";

    let parsedWordArray:any;
      if(data == null){
        console.log("data is null");
      }else{
          parsedWordArray = CryptoJS.enc.Base64.parse(data);
          parsedStr = parsedWordArray.toString(CryptoJS.enc.Utf8);
      }
            
      return parsedStr;
    }

    encryptData = function(data){
      var wordArray = CryptoJS.enc.Utf8.parse(data);
      var base64 = CryptoJS.enc.Base64.stringify(wordArray);

      return base64;
    }

}
