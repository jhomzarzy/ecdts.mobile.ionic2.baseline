import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

//providers
import { Base64converterProvider } from '../base64converter/base64converter'

/*
  Generated class for the LocalStorageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocalStorageProvider { 

  constructor(public http: Http, public base64Service: Base64converterProvider) {
    console.log('Hello LocalStorageProvider Provider');
  }
  getActiveUser = function (){
		var data = localStorage.getItem('activeuser');
		return JSON.parse(this.base64Service.decryptData(data));
	}

	getFvoOperator = function () {
		var data = localStorage.getItem('fvoOperator');
		return JSON.parse(this.base64Service.decryptData(data));
	}

	getSession = function(){
		var data = localStorage.getItem('session');
		return this.base64Service.decryptData(data);
	}

	getCreatedDate = function(){
		var data = localStorage.getItem('dateCreation');
		return this.base64Service.decryptData(data);
	}

	setSession = function(data){
		localStorage.setItem('session',this.base64Service.encryptData(data));
	}

	setActiveUser = function(data){
		localStorage.setItem('activeuser',this.base64Service.encryptData(JSON.stringify(data)));
	}

	setCreatedDate = function(data){
		localStorage.setItem('dateCreation',this.base64Service .encryptData(data));
	}

	

	setFvoOperator = function (data) {
		localStorage.setItem('fvoOperator', this.base64Service.encryptData(JSON.stringify(data)));
	}


	resetStorage = function(){
		localStorage.removeItem("session");
    localStorage.removeItem("activeuser");
		localStorage.removeItem("dateCreation");
	}

	setAuthToken = function(data){
		localStorage.setItem('auth',this.base64Service.encryptData(JSON.stringify(data)));
	}

	getAuthToken = function (){
		var data = localStorage.getItem('auth');
		if (data == null || data == undefined) return null;
		return JSON.parse(this.base64Service.decryptData(data));
	}

	removeAuthToken = function(){
		localStorage.removeItem('auth');
	}
}
