import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import  * as msgpacklite from 'msgpack-lite';
/*
  Generated class for the MsgpackLiteProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MsgpackLiteProvider {

  constructor(public http: Http) { 
    console.log('Hello MsgpackLiteProvider Provider');
  }

  encodeAttachment = function(data){
    return Array.prototype.slice.call(msgpacklite.encode(data));
  }

  decodeAttachment = function(data){
    // var obj = msgpack.decode(new Uint8Array(data))
    // return JSON.parse(obj);
    var attchList = msgpacklite.decode(new Uint8Array(data));
    var attchContainer = [];
    attchList.forEach( v => {
        attchContainer.push(JSON.parse(v));
    });
    return  attchContainer;
  }

}
