import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

//rest service
import { Rest } from '../../../app/services/rest';

//shared providers
import { LocalStorageProvider } from '../../../providers/shared-providers/local-storage/local-storage';

import { ConfigEnum } from '../../../app/services/Config';

/*
  Generated class for the LoginResourceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoginResourceProvider {

  constructor(public http: Http, private rest: Rest,
              private localstorageprovider: LocalStorageProvider) {
    console.log('Hello LoginResourceProvider Provider');
  }

  authenticate(user){
    return this.rest.data("POST",ConfigEnum.bfar_login_service+"/authenticate",user);
  }

  findCurrentUser(){
    let user = this.localstorageprovider.getActiveUser();
    return this.rest.data("GET",ConfigEnum.bfar_login_service+"/users?search=uuid="+user.uuid,null);
  }

  findUserInfo(){
    let user = this.localstorageprovider.getActiveUser();
    return this.rest.data("GET",ConfigEnum.bfar_login_service+"/userinformations/user/"+user.uuid,null);
  }

  findUserInfoByUserId(userId){
    return this.rest.data("GET",ConfigEnum.bfar_login_service+"/userinformations/user/"+userId,null);
  }

}
